<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;
use emilasp\json\models\JsonScheme;

class m151224_010003_AddDynamicFieldTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('json_scheme', [
            'id'         => $this->primaryKey(11),
            'code'       => $this->string(50)->notNull(),
            'name'       => $this->string(255)->notNull(),
            'text'       => $this->text(),
            'group'      => $this->string(50)->notNull(),
            'model'      => $this->string(250),
            'attribute'  => $this->string(50),
            'scheme'     => 'jsonb NULL DEFAULT \'{}\'',
            'history'    => 'jsonb NULL DEFAULT \'{}\'',
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_json_scheme_created_by',
            'json_scheme',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_json_scheme_updated_by',
            'json_scheme',
            'updated_by',
            'users_user',
            'id'
        );

        $this->insertBaseSchemes();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('json_scheme');

        $this->afterMigrate();
    }


    private function insertBaseSchemes()
    {
        JsonScheme::addScheme(
            'Телефон',
            'phone',
            'base',
            '[{"data": "{}", "hint": "", "type": "checkbox", "label": "вкл", "model": "{}", "default": "1", "enabled": 1, "attributes": "{}"}, {"data": "{}", "hint": "", "type": "phone", "label": "Телефон", "model": "{}", "default": "", "enabled": 1, "attributes": "{\"class\":\"phone form-control\"}"}, {"data": "{}", "hint": "", "type": "textarea", "label": "Комментарий", "model": "{}", "default": "", "enabled": 1, "attributes": "{\"class\":\"form-control\"}"}]'
        );

        JsonScheme::addScheme(
            'Города',
            'cities',
            'base',
            '[{"data": "{}", "hint": "", "type": "city", "label": "Город", "model": "{\"required\":[]}", "default": "", "enabled": 1, "attributes": "{}"}]'
        );

        JsonScheme::addScheme(
            'Класс',
            'class',
            'base',
            '[{"data": "{}", "hint": "", "type": "text", "label": "Класс", "model": "{}", "default": "", "enabled": 1, "attributes": "{}"}, {"data": "{}", "hint": "", "type": "text", "label": "Метод", "model": "{}", "default": "", "enabled": 1, "attributes": "{}"}, {"data": "{}", "hint": "", "type": "text", "label": "Опции", "model": "{}", "default": "", "enabled": 1, "attributes": "{}"}]'
        );

        JsonScheme::addScheme(
            'Способы оплаты',
            'payments',
            'links',
            '[{"data": "{\"class\":\"emilasp\\\\im\\\\common\\\\models\\\\Payment\",\"map\":[\"id\",\"name\"], \"condition\":[]}", "hint": "", "type": "relation", "label": "Наименование", "model": "{}", "default": "", "enabled": 1, "attributes": "{}"}]'
        );

    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
