<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\json\models\JsonScheme */

$this->title = Yii::t('json', 'Create Json Scheme');
$this->params['breadcrumbs'][] = ['label' => Yii::t('json', 'Json Schemes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="json-scheme-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
