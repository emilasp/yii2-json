<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\json\models\JsonScheme */

$this->title = Yii::t('json', 'Update {modelClass}: ', [
    'modelClass' => 'Json Scheme',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('json', 'Json Schemes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('json', 'Update');
?>
<div class="json-scheme-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
