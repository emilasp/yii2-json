v<?php

use dosamigos\ckeditor\CKEditor;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\json\models\JsonScheme */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="json-scheme-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-8">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-2">
            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'group')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'attribute')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="panel panel-success">
        <!-- Default panel contents -->
        <div class="panel-heading"><?= Yii::t('json', 'Scheme') ?></div>
        <div class="panel-body">
            <?= DynamicFields::widget([
                'form'      => $form,
                'model'     => $model,
                'attribute' => 'scheme',
                'scheme'    => DynamicModel::SCHEME_FIELDS,
            ]); ?>

        </div>
    </div>


    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset'  => 'standart',
    ]) ?>


    <?= $form->field($model, 'history')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('json', 'Create') : Yii::t('json', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
