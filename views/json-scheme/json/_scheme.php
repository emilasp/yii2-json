<?php
use yii\helpers\Html;
?>


<div class="item panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title float-md-left"><?= Yii::t('json', 'Schemes') ?></h3>

        <div class="float-md-right">
            <button type="button" class="add-item btn btn-success btn-xs"><i
                    class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">

        <?php foreach ($models as $i => $model) : ?>
            <div class="row row-item">
                <div class="col-sm-1">
                    <?= $form->field($model, "[{$i}]data")
                             ->textInput(['maxlength' => true, 'placeholder' => 'data'])->label(false) ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, "[{$i}]hint")
                        ->textInput(['maxlength' => true, 'placeholder' => 'hint'])->label(false) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, "[{$i}]type")
                             ->textInput(['maxlength' => true, 'placeholder' => 'type'])->label(false) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, "[{$i}]label")
                             ->textInput(['maxlength' => true, 'placeholder' => 'label'])->label(false) ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, "[{$i}]model")
                             ->textInput(['maxlength' => true, 'placeholder' => 'model'])->label(false) ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, "[{$i}]default")
                             ->textInput(['maxlength' => true, 'placeholder' => 'default'])->label(false) ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, "[{$i}]attributes")
                             ->textInput(['maxlength' => true, 'placeholder' => 'attributes'])->label(false) ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, "[{$i}]enabled")->checkbox()->label(false) ?>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="remove-item btn btn-danger btn-xs">
                        <i class="glyphicon glyphicon-minus"></i>
                    </button>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
