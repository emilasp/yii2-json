<?php
namespace emilasp\json\models;

use yii;

/**
 * Class DynamicModel
 * @package frontend\widgets\DynamicFields\model
 */
class DynamicModel extends yii\base\DynamicModel
{
    const SCHEME_PHONE   = 'phone';
    const SCHEME_ADDRESS = 'address';
    const SCHEME_CITY    = 'city';
    const SCHEME_FIELDS  = 'fields';
    const SCHEME_HANDLER = 'handler';
    const SCHEME_CUSTOM  = 'custom';

    public static $schemes = [
        self::SCHEME_PHONE   => [
            'phone'   => ['label' => 'phone', 'rules' => []],
            'comment' => ['label' => 'comment', 'rules' => []],
        ],
        self::SCHEME_ADDRESS => [
            'city'     => ['label' => 'City', 'rules' => [['required']]],
            'street'   => ['label' => 'Street', 'rules' => [['required']]],
            'house'    => ['label' => 'House', 'rules' => [['required']]],
            'build'    => ['label' => 'Build', 'rules' => []],
            'access'   => ['label' => 'Access', 'rules' => []],
            'intercom' => ['label' => 'Intercom', 'rules' => []],
            'office'   => ['label' => 'Office', 'rules' => []],
            'comment'  => ['label' => 'Address comment', 'rules' => []],
        ],
        self::SCHEME_CITY    => [
            'city_id' => ['label' => 'City', 'rules' => [['integer']]],
        ],
        self::SCHEME_HANDLER => [
            'class'  => ['label' => 'Class', 'rules' => [['required']]],
            'method' => ['label' => 'Method', 'rules' => [['required']]],
            'params' => ['label' => 'Params', 'rules' => []],
        ],
        self::SCHEME_FIELDS  => [
            'label'      => ['label' => 'Label', 'rules' => [['required']]],
            'type'       => ['label' => 'Type', 'rules' => [['required']]],
            'data'       => ['label' => 'Data', 'rules' => []],
            'default'    => ['label' => 'Default', 'rules' => []],
            'enabled'    => ['label' => 'Enabled', 'rules' => []],
            'attributes' => ['label' => 'Attrs', 'rules' => []],
            'hint'       => ['label' => 'Hint', 'rules' => []],
        ],
    ];


    public $formName;
    public $attributeLabels = [];


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return $this->attributeLabels;
    }

    public function init()
    {
        parent::init();

    }

    public function formName()
    {
        return $this->formName;
    }
}
