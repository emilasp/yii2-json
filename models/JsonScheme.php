<?php

namespace emilasp\json\models;

use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "json_scheme".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $text
 * @property string $group
 * @property string $model
 * @property string $attribute
 * @property string $scheme
 * @property string $history
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class JsonScheme extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'json_scheme';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'scheme',
                'scheme'    => DynamicModel::SCHEME_FIELDS,
            ],
            /*[
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'history',
            ],*/
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'status'], 'required'],
            [['text', 'scheme', 'history'], 'string'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['code', 'group', 'attribute'], 'string', 'max' => 50],
            [['name', 'model'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('json', 'ID'),
            'code'       => Yii::t('json', 'Code'),
            'name'       => Yii::t('json', 'Name'),
            'text'       => Yii::t('json', 'Text'),
            'group'      => Yii::t('json', 'Group'),
            'model'      => Yii::t('json', 'Model class'),
            'attribute'  => Yii::t('json', 'Attribute model'),
            'scheme'     => Yii::t('json', 'Scheme'),
            'history'    => Yii::t('json', 'History'),
            'status'     => Yii::t('json', 'Status'),
            'created_at' => Yii::t('json', 'Created At'),
            'updated_at' => Yii::t('json', 'Updated At'),
            'created_by' => Yii::t('json', 'Created By'),
            'updated_by' => Yii::t('json', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /** Добавляем новую схему
     *
     * @param $name
     * @param $code
     * @param $group
     * @param $scheme
     */
    public static function addScheme($name, $code, $group, $scheme)
    {
        $table = self::tableName();
        $sql = <<<SQL
INSERT INTO {$table} (code, name, text, "group", model, attribute, scheme, history, status, created_at, updated_at, created_by, updated_by)
VALUES ('{$code}', '{$name}', null, '{$group}', null, null, '{$scheme}', '[]', 1, NOW(), NOW(), 1, 1);
SQL;
        Yii::$app->db->createCommand($sql)->execute();
    }
}
