<?php

namespace emilasp\json\components;

use emilasp\json\models\DynamicModel;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\log\Logger;
use yii\validators\EmailValidator;

/**
 * Компонент обёртка для динамических моделей
 *
 * Class DynamicData
 * @package frontend\widgets\DynamicFields\components
 */
class DynamicData extends Component
{
    /** Префикс для POST данных модели */
    const PREFIX_MODEL = 'Dynamic';

    public $attribute = '';
    public $attributes = [];

    /** @var array Данные в виде массива ['attr' => 'value'] */
    public $values;

    public $isAction = false;


    public function init()
    {
        if (empty($this->attribute)) {
            throw new InvalidConfigException("The 'attribute' property must be set.");
        }
    }

    /** Формируем имя модели для POST запроса и динамического класса модели
     * @return string
     */
    public function getModelName()
    {
        return self::PREFIX_MODEL . ucfirst($this->attribute);
    }

    /**
     * Формируем модели из значений.
     * Заполняем переданные в POST значения
     */
    public function getModels()
    {
        $this->getRequestData();

        if (!is_array($this->values)) {
            $this->values = [];
        }

        $models = [];
        foreach ($this->values as $modelData) {
            $models[] = self::getModel($this->attribute, $this->attributes, $modelData);
        }
        return $models;
    }

    /** Формируем динамическую модель
     *
     * @param array      $attributes
     * @param null|array $values
     *
     * @return DynamicModel
     */
    public static function getModel(string $attribute, array $attributes = [], $values = null)
    {
        $model           = new DynamicModel(array_keys($attributes));
        $model->formName = self::PREFIX_MODEL . ucfirst($attribute);

        foreach ($attributes as $attr => $struct) {
            $label = $struct['label'];
            $rules = $struct['rules'];
            foreach ($rules as $rule) {
                $model->addRule([$attr], $rule[0], (isset($rule[1]) ? $rule[1] : []));
            }

            $model->attributeLabels[$attr] = Yii::t('site', $label);

            //$model->{$attr} = is_array($values) ? $values[$attr] : $values;
            if (isset($values[$attr])) {
                $model->{$attr} = $values[$attr];
            }
        }
        return $model;
    }

    /** Получаем знаения полей из запроса
     * @return array|mixed|null
     */
    private function getRequestData()
    {
        if (Yii::$app->request instanceof \yii\console\Request) {
            return null;
        }

        if (!Yii::$app->request->isPost) {
            return null;
        }

        $modelsData = [];
        $postData = Yii::$app->request->post($this->getModelName(), []);

        if (isset($postData['data'])) {
            $data       = urldecode($postData['data']);
            $data       = urldecode($data);
            $modelsData = json_decode($data, true);

            $action = $postData['action'];
            if ($action === 'insert') {
                $modelsData[]   = array_fill_keys(array_keys($this->attributes), '');
                $this->isAction = true;
            }
        } else {
            foreach ($postData as $row) {
                $modelsData[] = $row;
            }
        }
        $this->values = $modelsData;
    }
}
