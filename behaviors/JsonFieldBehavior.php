<?php
namespace emilasp\json\behaviors;

use emilasp\json\components\DynamicData;
use emilasp\json\models\DynamicModel;
use yii;
use yii\base\Behavior;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

/** Получаем и сохраняем данные из поля json модели
 * Class JsonFieldBehavior
 * @package emilasp\json\behaviors
 */
class JsonFieldBehavior extends Behavior
{
    /** Имя поля в БД
     * @var string
     */
    public $attribute;
    
    public $scheme;
    public $defaultValue;

    private $dynamicComponent;
    private $models;

    /**
     * EVENTS
     *
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_INIT => 'setDefault',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'setNull',
            ActiveRecord::EVENT_BEFORE_UPDATE   => 'setField',
            ActiveRecord::EVENT_BEFORE_INSERT   => 'setField',
        ];
    }

    /**
     *  INIT
     */
    public function init(): void
    {
        parent::init();

        if (is_string($this->scheme)) {
            $this->scheme = DynamicModel::$schemes[$this->scheme];
        }
    }


    /**
     * Устанавливаем значение по уолчанию
     */
    public function setDefault(): void
    {
        if ($this->defaultValue && $this->owner->isNewRecord) {
            $this->owner->{$this->attribute} = json_encode($this->defaultValue);
        }
    }

    /**
     * Устанавливаем пустой json
     *
     * @return bool
     */
    public function setNull(): bool
    {
        //if (!$this->owner->search) {
            if (!$this->owner->{$this->attribute}) {
                $this->owner->{$this->attribute} = '[]';
            }
            $this->validateField();
        //}
        return true;
    }

    /**
     * Формируем динамические модели
     */
    public function setModels(): void
    {
        if (!$this->dynamicComponent) {
            $this->dynamicComponent = new DynamicData([
                'attribute'  => $this->attribute,
                'attributes' => $this->scheme,
                'values'     => (array)$this->owner->{$this->attribute},
            ]);
        }
        $this->models = $this->dynamicComponent->getModels();
    }

    /**
     * Валидируем динамические модели
     */
    public function validateField(): void
    {
        $this->setModels();

        if (Yii::$app->request instanceof yii\console\Request) {
            return;
        }

        Model::loadMultiple($this->models, Yii::$app->request->post());
        $errors = ActiveForm::validateMultiple($this->models);

        if (count($errors)) {
            $this->owner->addError($this->attribute, '');
        }
    }

    /** Валидируем поля и
     * Устанавливаем поле в родительской моделе
     */
    public function setField(): void
    {
        $this->validateField();

        $dataToSave = [];
        foreach ($this->models as $model) {
            $modelToSave = [];
            foreach (array_keys($this->scheme) as $attribute) {
                $modelToSave[$attribute] = $model->{$attribute};
            }
            $dataToSave[] = $modelToSave;
        }

        $this->owner->{$this->attribute} = json_encode($dataToSave);
    }

    /** Получаем модели
     * @return mixed
     */
    private function getData()
    {
        $this->setModels();
        return $this->models;
    }


    /** Проверяем на существование свойства
     *
     * @param string $name
     * @param bool|true $checkVars
     *
     * @return bool
     */
    public function canGetProperty($name, $checkVars = true): bool
    {
        if ($this->checkName($name)) {
            return true;
        }
        return parent::canGetProperty($name, $checkVars);
    }

    /** Геттер
     *
     * @param string $name
     *
     * @return array|mixed
     */
    public function __get($name)
    {
        if ($this->checkName($name)) {
            return $this->getData();
        }
        return parent::__get($name);
    }

    /** Проверяем что это разрешённое свойство
     *
     * @param $name
     *
     * @return bool
     */
    private function checkName($name): bool
    {
        $nameDiff = Inflector::pluralize($this->attribute);
        if ($nameDiff === $name) {
            return true;
        }
        return false;
    }
}
