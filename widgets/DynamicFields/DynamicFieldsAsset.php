<?php
namespace emilasp\json\widgets\DynamicFields;

use yii\web\AssetBundle;

/**
 * Class DynamicFieldsAsset
 * @package frontend\widgets\DynamicFields
 */
class DynamicFieldsAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $js         = ['dynamic.js'];
    public $css        = ['style.css'];

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
    ];
}
