(function ($) {

    var methods = {
        init: function (settings, $this) {
            $(document).off('click', settings.selectorMain + ' ' + settings.selectorAdd);
            $(document).on('click', settings.selectorMain + ' ' + settings.selectorAdd, function () {
                methods.add(settings);
            });

            $(document).off('click', settings.selectorMain + ' ' + settings.selectorRemove);
            $(document).on('click', settings.selectorMain + ' ' + settings.selectorRemove, function () {
                if (confirm('Удалить?')) {
                    $(this).closest(settings.selectorRow).remove();
                }
            });

            if (settings.sort) {
                $(settings.selectorMain + ' ' + settings.selectorRow).parent().sortable({
                    update: function() {
                        $(settings.selectorMain + ' ' + settings.selectorRow).each(function(index, row) {
                            var rowObj = $(row);
                            rowObj.find('select, input').each(function(i, field) {
                                var el = $(field);
                                el.attr('name', el.attr('name').replace(new RegExp(/(\d+)/), index));
                            });
                        });
                    },
                    delay: 30
                });
            }

        },
        add: function (settings) {
            var data = methods._buildData(settings);
            var objectSend = {};
            objectSend[settings.modelName] = {"action": "insert", "data": data};

            $.pjax({
                container: settings.selectorMain,
                "timeout": 0,
                "method": "POST",
                "scrollTo": false,
                "data": objectSend,
                push: false
            });

        },
        _getValue: function (settings, el, set) {
            set = typeof set !== 'undefined' ? set : false;

            var elObj = $(el);
            var value = '';

            if (elObj.is(':checkbox') || elObj.is(':radio')) {
                //var type = (elObj.is(':checkbox')) ? 'checkbox' : 'radio';
                if (set && elObj.is(':checked')) {
                    elObj.val(set);
                } else {
                    value = elObj.is(':checked') ? 1 : 0;
                }
            } else {
                if (set) {
                    elObj.val(set);
                } else {
                    value = elObj.val();
                }
            }
            return value;
        },
        _buildData: function (settings) {
            var obj = {};

            var rows = $(settings.selectorMain + ' ' + settings.selectorRow);

            rows.each(function (indexRow, elRow) {
                obj[indexRow] = {};
                $(elRow).find('input, textarea, select').each(function (indexInput, elInput) {
                    var field = $(elInput);
                    var id = field.attr('id');

                    if (id) {
                        var codeArr = id.toString().split('-');

                        var code  = codeArr[codeArr.length - 1];
                        var value   = methods._getValue(settings, field);

                        obj[indexRow][code] = value;
                    }

                });
            });

            return encodeURIComponent(JSON.stringify(obj));
        }
    };


    /**
     * Формируем и работаем с динамической моделью
     * @param options
     */
    $.fn.yiiDynamicFields = function (options) {
        var $this = this;

        var settings = $.extend({
            'modelName': '',
            'selectorMain': '.main-rows',
            'selectorRow': '.row-item',
            'selectorAdd': '.add-item',
            'selectorRemove': '.remove-item'
        }, options);

        methods.init(settings, $this);
    };


})(window.jQuery);