<?php
use yii\helpers\Html;
use yii\widgets\MaskedInput;

?>


<div class="item panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title float-md-left"><?= Yii::t('site', 'Phones') ?></h3>

        <div class="float-md-right">
            <button type="button" class="add-item btn btn-success btn-xs"><i
                    class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">

        <?php foreach ($models as $i => $model) : ?>
            <div class="row row-item">
                <div class="col-sm-4">
                    <?= $form->field($model, "[{$i}]phone")
                        ->widget(MaskedInput::className(), ['mask' => '+7 (999) 999-99-99'])->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, "[{$i}]comment")
                             ->textInput(['maxlength' => true, 'placeholder' => 'comment'])->label(false) ?>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="remove-item btn btn-danger btn-xs">
                        <i class="glyphicon glyphicon-minus"></i>
                    </button>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
