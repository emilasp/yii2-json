<?php
use emilasp\geoapp\models\GeoKladr;
use emilasp\geoapp\models\GeoStreet;
use emilasp\geoapp\widgets\SelectCityWidget\SelectCityWidget;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

?>

    <!--
    'city'    => ['label' => Yii::t('im', 'City'), 'rules' => []],
    'street'  => ['label' => Yii::t('im', 'Street'), 'rules' => []],
    'house'   => ['label' => Yii::t('im', 'House'), 'rules' => []],
    'build'   => ['label' => Yii::t('im', 'Build'), 'rules' => []],
    'office'  => ['label' => Yii::t('im', 'Office'), 'rules' => []],
    'comment' => ['label' => Yii::t('im', 'Address comment'), 'rules' => []],
    -->

    <div class="item panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title float-md-left"><?= Yii::t('site', 'Address') ?></h3>

            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <?php $model = $models[0] ?>

            <div class="row">
                <div class="col-md-6">

                    <?php
                    $resultsJs = <<< JS
function (data, params) {
console.log(data);
    params.page = params.page || 1;
    return {
        results: data.results,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

                    $valueCity = '';
                    if ($model->city) {
                        $city      = GeoKladr::findOne($model->city);
                        $valueCity = $city->name_full;
                    }
                    ?>
                    <?= $form->field($model, "[0]city")->widget(Select2::className(), [
                        'id'            => 'geo-city-select',
                        'language'      => 'ru',
                        'name'          => 'geo-city-select',
                        'value'         => $model->city,
                        'initValueText' => $valueCity,
                        'options'       => ['placeholder' => Yii::t('site', 'Search city')],
                        'pluginOptions' => [
                            'allowClear'         => true,
                            'minimumInputLength' => 1,
                            'ajax'               => [
                                'url'            => Url::toRoute('/geo/kladr/search-city'),
                                'dataType'       => 'json',
                                'delay'          => 250,
                                'data'           => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache'          => true,
                            ],
                            'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                            'templateResult'     => new JsExpression('formatCitySelect'),
                        ],
                        'pluginEvents'  => [
                            'change' => 'function(event) { setStreetToAdress(event); }',
                        ],
                    ]) ?>

                    <?php Pjax::begin(['id' => 'address-street']); ?>
                    <?php
                    $streets          = [];
                    $streetParentCode = Yii::$app->request->post(
                        'street_parent_code',
                        $model->city
                    );
                    if ($streetParentCode) {
                        $parentStreet = GeoKladr::findOne(['id' => $streetParentCode]);

                        if ($parentStreet) {
                            $streets = GeoStreet::find()->select('name')->where("code like '" . $parentStreet->code . "%'")
                                                ->orderBy('name')->asArray()->column();
                        }
                    }
                    /*
                                        $valueStreet = '';
                                        $mapedStreets = \yii\helpers\ArrayHelper::map($streets ,'id', 'text');
                                        if ($model->street && isset($mapedStreets[$model->street])) {
                                            $valueStreet = $mapedStreets[$model->street];
                                        }*/
                    ?>

                    <?= $form->field($model, '[0]street')->widget(\yii\jui\AutoComplete::classname(), [
                        'options'       => [
                           /* 'id'          => 'geo-street-select',
                            'name'        => 'geo-street-select',*/
                            'class'       => 'form-control',
                            'placeholder' => Yii::t('site', 'Search street'),
                        ],
                        'clientOptions' => [
                            'source'    => $streets,
                            'autoFill'  => true,
                            'minLength' => '0',
                        ],
                    ]) ?>

                    <?php Pjax::end(); ?>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, "[0]house")->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, "[0]build")->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, "[0]access")->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, "[0]office")->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, "[0]intercom")->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, "[0]comment")->textarea(['rows' => 6]) ?>
                </div>
            </div>

        </div>
    </div>

<?php
$urlToSearchStreet = Url::toRoute('/geo/kladr/get-street');
$js                = <<<JS
    function setStreetToAdress() {
        var parent_code = $('#dynamicaddress-0-city').val();
        console.log(parent_code);
        $.pjax.reload({
            container:"#address-street",
            "timeout" : 0,
            //url: "{$urlToSearchStreet}",
            method:"POST",
            push:false,
            "data":{"street_parent_code":parent_code}
          });
       
    }
JS;

$this->registerJs($js, \yii\web\View::POS_HEAD);
