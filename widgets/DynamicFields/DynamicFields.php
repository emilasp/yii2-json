<?php
namespace emilasp\json\widgets\DynamicFields;

use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\json\components\DynamicData;
use emilasp\json\models\DynamicModel;
use yii;
use yii\base\InvalidConfigException;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\widgets\Pjax;

/**
 * Динамические поля
 *
 * Для модели добавляем поведение(я)
 *
 *   [
 *       'class'     => JsonFieldBehavior::className(),
 *       'attribute' => 'phone',
 *       'scheme'    => [
 *           'phone'   => [
 *               'label' => 'Телефон',
 *               'rules' => [['string', ['min' => 2, 'max' => 16]]]
 *           ],
 *           'comment' => [
 *               'label' => 'Комментарий',
 *               'rules' => [['required', []], ['string', ['min' => 2, 'max' => 16]]]
 *           ]
 *       ],
 *   ],
 * Добавляем новую вьюху с соблюдением структуры:
 *
 *<div class="item panel panel-default"><!-- widgetBody -->
 *    <div class="panel-heading">
 *        <h3 class="panel-title float-md-left">Address</h3>
 *        <div class="float-md-right">
 *            <button type="button" class="add-item btn btn-success btn-xs"><i
 *                    class="glyphicon glyphicon-plus"></i></button>
 *        </div>
 *        <div class="clearfix"></div>
 *    </div>
 *    <div class="panel-body">
 *      <?php foreach ($models as $i => $model) : ?>
 *            <?php
 *            if (!$model instanceof DynamicModel && !$model->isNewRecord) { // necessary for update action.
 *                echo Html::activeHiddenInput($model, "[{$i}]id");
 *            }
 *            ?>
 *
 *            <div class="row row-item">
 *                <div class="col-sm-5">
 *                    <?= $form->field($model,
 *                        "[{$i}]phone")->widget(\yii\widgets\MaskedInput::className(), [
 *                        'mask' => '999-999-9999',
 *                    ]) ?>
 *                </div>
 *                <div class="col-sm-5">
 *                    <?= $form->field($model,
 *                        "[{$i}]comment")->textInput(['maxlength' => true]) ?>
 *                </div>
 *                <div class="col-sm-2">
 *                    <button type="button" class="remove-item btn btn-danger btn-xs">
 *                        <i class="glyphicon glyphicon-minus"></i>
 *                    </button>
 *                </div>
 *            </div>
 *        <?php endforeach ?>
 *  </div>
 *</div>
 *
 * В форме модели родителя вызываем виджет:
 *
 * <?= DynamicFields::widget([
 *     'form'      => $form,
 *     'model'     => $model,
 *     'attribute' => 'email',
 *     'viewFile'  => '@frontend/views/amulex-user/tabs/_emails',
 * ]); ?>
 *
 * Class JsonField
 * @package frontend\widgets\JsTree
 */
class DynamicFields extends yii\base\Widget
{
    public $sort = true;

    public $scheme;

    public $addEmptyRow = false;

    public $form;
    public $model;

    public $viewFile;

    public $attribute;
    public $attributes = [];

    /** Selectors */
    public $selectorRow = '.row-item';

    public $selectorAdd = '.add-item';
    public $selectorRemove = '.remove-item';
    public $selectorMain = '#main-rows-';

    private $modelName;

    private $models;

    public function init()
    {
        parent::init();

        $this->checkConfig();

        $this->setDefaults();

        $this->setViewFileByScheme();

        $this->registerAssets();

        $this->setRows();
    }

    public function run()
    {
        Pjax::begin(['id' => $this->id]);

        echo $this->renderFile($this->viewFile, ['form' => $this->form, 'models' => $this->models]);

        if (is_array($this->model->{$this->attribute})) {
            $this->model->{$this->attribute} = json_encode($this->model->{$this->attribute});
        }

        echo $this->form->field($this->model, $this->attribute)->hiddenInput()->label(false);

        Pjax::end();
    }

    /**
     * Формирууем модели
     */
    private function setRows()
    {
        $this->models = $this->model->{Inflector::pluralize($this->attribute)};

        if (!count($this->models) && $this->addEmptyRow) {
            $modelAtributes = $this->getBehaviorScheme();

            $model = DynamicData::getModel($this->attribute, $modelAtributes);

            $model->setAttributes($this->attributes);

            $this->models[] = $model;
        }
    }

    /**
     * Получаем актуальную схему
     *
     * @return array
     */
    private function getBehaviorScheme()
    {
        $scheme = [];
        foreach ($this->model->behaviors as $index => $behavior) {
            if ($behavior instanceof JsonFieldBehavior && $this->attribute === $behavior->attribute) {
                $scheme = $behavior->scheme;
                continue;
            }
        }
        return $scheme;
    }

    /**
     * Устанавливаем атрибуты по умолчанию
     */
    private function setDefaults()
    {
        $this->selectorMain .= $this->attribute;
        $this->id        = str_replace('#', '', $this->selectorMain);
        $this->modelName = DynamicData::PREFIX_MODEL . ucfirst($this->attribute);
    }

    /**
     * Устанавливаем вью взависимости от типа
     */
    private function setViewFileByScheme()
    {
        if ($this->scheme === DynamicModel::SCHEME_CUSTOM) {
            $this->viewFile = Yii::getAlias($this->viewFile . '.php');
        } else {
            $this->viewFile = $this->viewPath . DIRECTORY_SEPARATOR . '_' . $this->scheme . '.php';
        }
    }

    /** Проверяем что все обязательные настройки виджета заполнены
     * @throws InvalidConfigException
     */
    private function checkConfig()
    {
        if (empty($this->attribute)) {
            throw new InvalidConfigException("The 'attribute' property must be set.");
        }

        if (empty($this->scheme)) {
            throw new InvalidConfigException("The 'scheme' property must be set.");
        }

        if (empty($this->form)) {
            throw new InvalidConfigException("The 'form' property must be set.");
        }
        if (empty($this->model)) {
            throw new InvalidConfigException("The 'model' property must be set.");
        }
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        DynamicFieldsAsset::register($view);
        $this->registerJs();
    }

    /**
     */
    private function registerJs()
    {
        $options = Json::encode([
            'sort'           => $this->sort,
            'modelName'      => $this->modelName,
            'selectorMain'   => $this->selectorMain,
            'selectorRow'    => $this->selectorRow,
            'selectorAdd'    => $this->selectorAdd,
            'selectorRemove' => $this->selectorRemove,
        ]);

        $js = <<<JS
        $('{$this->selectorMain}').yiiDynamicFields({$options});
JS;
        $this->view->registerJs($js);
    }
}
