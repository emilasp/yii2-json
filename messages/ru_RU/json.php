<?php
return [
    'Type string'   => 'Строка',
    'Type integer'  => 'Число',
    'Type checkbox' => 'Чекбокс',
    'Type radio'    => 'Радио',
    'Type select'   => 'Селект',
    'Type select2'  => 'Селект2',
];
